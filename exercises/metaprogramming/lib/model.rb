# frozen_string_literal: true

# BEGIN
require 'date'

module Model
  def self.included(base)
    base.extend ClassMethods
    base.attr_reader :attributes
  end

  module ClassMethods
    def defined_attributes
      @defined_attributes || []
    end

    def attribute(name, options)
      @defined_attributes ||= []
      @defined_attributes << name

      define_method name do
        instance_variable_get "@#{name}"
      end

      define_method "#{name}=" do |value|
        converted_value = convert(value, options[:type])
        instance_variable_set "@#{name}", converted_value
        converted_value
      end
    end
  end

  def initialize(attributes = {})
    @attributes = {}
    self.class.defined_attributes.each do |attribute|
      if attributes.key? attribute
        @attributes[attribute] = send("#{attribute}=", attributes[attribute])
      else
        @attributes[attribute] = nil
      end
    end
  end

  private

  def convert(item, desired_type)
    return if item.nil?

    case desired_type
    when :string
      item.to_s
    when :integer
      item.to_i
    when :datetime
      DateTime.parse(item)
    when :boolean
      if item.instance_of?(TrueClass) || item.instance_of?(FalseClass)
        item
      else
        to_boolean(item)
      end
    end
  end

  def to_boolean(item)
    return unless item.instance_of? String

    case item.downcase
    when 'yes' || 'true'
      true
    when 'no' || 'false'
      false
    end
  end
end

# END
