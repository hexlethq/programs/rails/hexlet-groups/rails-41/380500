# frozen_string_literal: true

# BEGIN

def get_same_parity(arr)
  return [] if arr.empty?

  if arr.first.odd?
    arr.filter(&:odd?)
  else
    arr.filter(&:even?)
  end
end

# END
