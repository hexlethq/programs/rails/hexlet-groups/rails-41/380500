require 'csv'

namespace :hexlet do
  desc 'Imports users from a CSV file'
  task :import_users, [:file_path] => :environment do |t, args|
    path = args[:file_path]
    abort 'Data path is required!' unless path
    puts path
    abort 'Cant find data file!' unless File.exist?(path) && File.file?(path)
    CSV.foreach(path, headers: true) do |row|
      User.create(
        first_name: row['first_name'],
        last_name: row['last_name'],
        birthday: row['birthday'],
        email: row['email']
      )
    end
  end
end
