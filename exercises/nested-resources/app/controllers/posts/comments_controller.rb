class Posts::CommentsController < Posts::ApplicationController
  before_action :set_post_comment, only: %i[ edit update destroy ]
  before_action :resource_post, only: %i[ create ]

  def edit
  end

  def create
    comment = @resource_post.comments.build(post_comment_params)
    if comment.save
      redirect_to post_path(comment.post), notice: "Post comment was successfully created."
    else
      redirect_to post_path(comment.post), status: :unprocessable_entity
    end
  end

  def update
    if @post_comment.update(post_comment_params)
      redirect_to post_path(@post_comment.post), notice: 'Comment was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @post_comment.destroy
    redirect_to post_path(@post_comment.post), notice: "Post comment was successfully destroyed."
  end

  private

  def set_post_comment
    @post_comment = PostComment.find(params[:id])
  end

  def post_comment_params
    params.require(:post_comment).permit(:body)
  end
end
