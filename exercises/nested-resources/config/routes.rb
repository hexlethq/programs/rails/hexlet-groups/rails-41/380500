# frozen_string_literal: true

Rails.application.routes.draw do
  root 'homes#index'

  resources :posts do
    scope module: 'posts', shallow: true do
      resources :comments, only: [:edit, :create, :update, :destroy]
    end
  end
end
