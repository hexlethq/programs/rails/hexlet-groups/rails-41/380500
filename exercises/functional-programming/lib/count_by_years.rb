# frozen_string_literal: true

# BEGIN

def count_by_years(users)
  male_users = users.filter { |user| user[:gender] == 'male' }
  male_users.inject({}) do |result, user|
    year_of_birth = user[:birthday].split('-').first
    result[year_of_birth] = result.fetch(year_of_birth, 0) + 1
    result
  end
end

# END
