# frozen_string_literal: true

class SetLocaleMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    request = Rack::Request.new(env)
    header = request.env['HTTP_ACCEPT_LANGUAGE']
    locale = extract_locale_from_accept_language_header(header)
    Rails.logger.debug("#{locale} locale was found in HTTP_ACCEPT_LANGUAGE header")
    I18n.locale = I18n.available_locales.include?(locale) ? locale : I18n.default_locale
    Rails.logger.debug("#{I18n.locale} locale was set")
    @status, @headers, @response = @app.call(env)
    [@status, @headers, @response]
  end

  private

  def extract_locale_from_accept_language_header(header)
    if header
      header.scan(/^[a-z]{2}/).first.to_sym
    else
      I18n.default_locale
    end
  end
end
