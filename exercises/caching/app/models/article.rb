# frozen_string_literal: true

class Article < ApplicationRecord
  # BEGIN
  def last_reading_date
    Rails.cache.fetch("#{cache_key_with_version}/updated_at", expires_in: 12.hours) do
      updated_at
    end
  end
  # END
end
