# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  def setup
    @post = posts(:one)
    @comment = post_comments(:one)
  end

  #
  # posts tests
  #
  test 'should show posts' do
    visit posts_path
    assert_selector 'h1', text: 'Posts'
  end

  test 'should create post' do
    visit posts_path
    click_on 'New Post'
    fill_in 'Title', with: 'Creating a Post'
    fill_in 'Body', with: 'Created this post successfully!'
    click_on 'Create Post'
    assert_text 'Post was successfully created.'
  end

  test 'should show post' do
    visit post_path(@post)
    assert_text 'Body:'
  end

  test 'should edit post' do
    visit edit_post_path(@post)
    fill_in 'Title', with: 'Updated Title'
    click_on 'Update Post'
    assert_text 'Post was successfully updated.'
  end

  test 'should delete post' do
    visit posts_path
    accept_alert do
      click_on 'Destroy', match: :first
    end
    assert_text 'Post was successfully destroyed.'
  end

  #
  # posts/comments tests
  #
  test 'should show comments' do
    visit post_path(@post)
    assert_text 'One comment'
  end

  test 'should create comment' do
    visit post_path(@post)
    fill_in with: 'New comment', id: 'post_comment_body'
    click_on 'Create Comment'
    assert_text 'Comment was successfully created.'
  end

  test 'should edit comment' do
    visit post_path(@post)
    click_on 'Edit', match: :first
    fill_in 'Body', with: 'Updated comment'
    click_on 'Update Comment'
    assert_text 'Comment was successfully updated.'
  end

  test 'should delete comment' do
    visit post_path(@post)
    accept_alert do
      click_on 'Delete', match: :first
    end
    assert_text 'Comment was successfully deleted.'
  end
end
# END
