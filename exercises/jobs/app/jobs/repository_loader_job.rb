# frozen_string_literal: true

class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(repo_id)
    repo = Repository.find(repo_id)
    repo.fetch! if repo.may_fetch?
    repo_full_name = repo.link.delete_prefix('https://github.com/')

    client = Octokit::Client.new
    metadata = client.repo(repo_full_name)

    if repo.update(repo_params(metadata)) && repo.may_mark_as_fetched?
      repo.mark_as_fetched!
    elsif repo.may_fail?
      repo.fail!
    end
  rescue Octokit::NotFound
    repo.fail! if repo.may_fail?
  end

  private

  def repo_params(repo_metadata)
    {
      link: repo_metadata['html_url'],
      owner_name: repo_metadata['owner']['login'],
      repo_name: repo_metadata['name'],
      description: repo_metadata['description'],
      default_branch: repo_metadata['default_branch'],
      watchers_count: repo_metadata['watchers_count'],
      language: repo_metadata['language'],
      repo_created_at: repo_metadata['created_at'],
      repo_updated_at: repo_metadata['updated_at']
    }
  end
end
