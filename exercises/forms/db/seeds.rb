10.times do
  Post.create(
    title: Faker::DcComics.title,
    body: Faker::Lorem.paragraph,
    summary: Faker::Coffee.notes,
    published: [true, false].sample
  )
end
