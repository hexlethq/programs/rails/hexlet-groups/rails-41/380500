class AddDefaultValuesCompletedStatusCreator < ActiveRecord::Migration[6.1]
  def change
    change_column_default :tasks, :status, 'New'
    change_column_default :tasks, :creator, 'Current user'
    change_column_default :tasks, :completed, false
  end
end
