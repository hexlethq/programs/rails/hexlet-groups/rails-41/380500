class Post < ApplicationRecord
  validates :title, :summary, :body, presence: true
  validates :published, inclusion: [true, false]
end
