# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, response = @app.call(env)
    response_hash = Digest::SHA256.hexdigest(response)
    [status, headers, "#{response}\n#{response_hash}"]
  end
end
