# frozen_string_literal: true

class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    start_time = Time.now
    status, headers, response = @app.call(env)
    time_taken = (Time.now - start_time) * 1000
    [status, headers, "#{response}\n#{time_taken}"]
  end
end
