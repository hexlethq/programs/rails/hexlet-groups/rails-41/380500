# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

['To Do', 'In Progress', 'Done'].each do |status|
  Status.create(name: status)
end

5.times do |index|
  User.create(name: Faker::Name.unique.name)
end

10.times do
  Task.create(
    name: Faker::Lorem.sentence,
    description: Faker::Lorem.paragraph,
    status: Status.all.sample,
    user: User.all.sample
  )
end
