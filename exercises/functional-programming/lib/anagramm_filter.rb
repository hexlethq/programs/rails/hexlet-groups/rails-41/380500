# frozen_string_literal: true

# BEGIN

def anagramm_filter(word, anagramm_array)
  return [] if anagramm_array.empty?

  sorted_word = word.chars.sort.join
  anagramm_array.filter do |element|
    sorted_word == element.chars.sort.join
  end
end

# END
