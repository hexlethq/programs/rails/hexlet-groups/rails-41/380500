# frozen_string_literal: true

# BEGIN

def reverse(text)
  result = ''
  last_index = text.length - 1
  last_index.downto(0) do |index|
    result += text[index]
  end
  result
end

# END
