# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  words = text.split.map do |word|
    if stop_words.include? word
      '$#%!'
    else
      word
    end
  end
  words.join ' '
end

# rubocop:enable Style/For
