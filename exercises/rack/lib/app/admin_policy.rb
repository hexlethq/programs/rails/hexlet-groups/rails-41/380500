# frozen_string_literal: true

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)
    return [403, { 'Content-Type' => 'text/plain' }, 'Forbidden'] if req.path_info[%r{^/admin}]

    @app.call(env)
  end
end
