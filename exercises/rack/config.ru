# frozen_string_literal: true

# require 'rack/unreloader'
require_relative 'lib/app'

# Unreloader = Rack::Unreloader.new{App.init}
# Unreloader.require './lib/app.rb'
# Unreloader.require './lib/app/*.rb'

Rack::Handler::Thin.run App.init, Port: 3000, Host: '0.0.0.0'
# Rack::Handler::Thin.run Unreloader, Port: 3000, Host: '0.0.0.0'
