# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  test 'should get index' do
    get repositories_url
    assert_response :success
  end

  test 'should create repository' do
    repo_link = 'https://github.com/hexlet-basics/hexlet-basics'
    repo_full_name = repo_link.delete_prefix('https://github.com/')
    stubbed_response = load_fixture('../fixtures/files/response.json')
    stubbed_response_json = JSON.parse(stubbed_response)
    stub_request(:get, "https://api.github.com/repos/#{repo_full_name}")
      .to_return(status: 200, body: stubbed_response, headers: { 'Content-Type': 'application/json' })

    post repositories_url, params: {
      repository: { link: repo_link }
    }
    assert_response :redirect

    repository = Repository.find_by(link: stubbed_response_json['html_url'])
    assert repository
    assert repository.description == stubbed_response_json['description']
    assert repository.watchers_count == stubbed_response_json['watchers_count']
  end

  test 'should get new' do
    get new_repository_url
    assert_response :success
  end

  test 'should get edit' do
    repo = repositories(:one)
    get edit_repository_url(repo)
    assert_response :success
  end

  test 'should show repository' do
    repo = repositories(:one)
    get repository_url(repo)
    assert_response :success
  end

  test 'should update repository' do
    repo = repositories(:one)
    patch repository_url(repo), params: {
      repository: { link: 'https://newlink' }
    }
    assert_redirected_to repositories_path
  end

  test 'should delete repository' do
    repo = repositories(:one)
    delete repository_url(repo)
    assert_redirected_to repositories_path, 'Success'
  end
  # END
end
