# frozen_string_literal: true

# rubocop:disable Style/For
def build_query_string(params)
  return unless params.instance_of?(Hash)

  sorted_params = params.sort.to_h
  result = []
  sorted_params.each do |key, value|
    result.append "#{key}=#{value}"
  end
  result.join('&')
end
# rubocop:enable Style/For
