# frozen_string_literal: true

# BEGIN
require 'forwardable'
require 'uri'

class Url
  extend Forwardable
  include Comparable

  def initialize(url)
    @url = URI(url)
  end

  def <=>(other)
    @url.to_s <=> other.to_s
  end

  def query_params
    return if @url.query.nil?

    result = {}
    @url.query.split('&').each do |param|
      k, v = param.split('=')
      result[k.to_sym] = v
    end
    result
  end

  def query_param(key, default_value = nil)
    query_params.fetch(key, default_value)
  end

  def_delegators :@url, :scheme, :host, :to_s
end
# END
