# frozen_string_literal: true

# BEGIN

def fibonacci(num)
  # since the index of the first element is 0 and not 1,
  # we should substract 1 from n
  num -= 1
  fibonacci_sequence = [0, 1]
  return nil if num.negative?
  return fibonacci_sequence[num] if num < 2

  # 2 is the next index in fibonacci_sequence
  2.upto(num) do |index|
    fibonacci_sequence.append(
      fibonacci_sequence[index - 1] + fibonacci_sequence[index - 2]
    )
  end

  fibonacci_sequence[num]
end

# END
