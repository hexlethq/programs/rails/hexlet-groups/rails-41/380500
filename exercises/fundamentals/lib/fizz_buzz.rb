# frozen_string_literal: true

# BEGIN

def fizz_buzz(start, stop)
  return '' if start > stop

  result = ''
  start.upto(stop) do |n|
    result += if (n % 3).zero? && (n % 5).zero?
                'FizzBuzz'
              elsif (n % 3).zero?
                'Fizz'
              elsif (n % 5).zero?
                'Buzz'
              else
                n.to_s
              end
    result += ' ' if n != stop
  end
  result
end
# END
