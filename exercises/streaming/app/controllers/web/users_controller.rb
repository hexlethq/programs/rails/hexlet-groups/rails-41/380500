# frozen_string_literal: true

require 'csv'

class Web::UsersController < Web::ApplicationController
  def index
    @users = User.page(params[:page])
  end

  def show
    @user = User.find params[:id]
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find params[:id]
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: t('success')
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    @user = User.find params[:id]
    if @user.update(user_params)
      redirect_to @user, notice: t('success')
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find params[:id]
    @user.destroy
    redirect_to users_url, notice: t('success')
  end

  def normal_csv
    respond_to do |format|
      format.csv do
        csv = generate_csv(User.column_names, User.all)
        send_data(csv)
      end
    end
  end

  # BEGIN
  def stream_csv
    respond_to do |format|
      format.csv do
        # delete this header so that Rack knows to stream the content
        headers.delete('Content-Length')
        # do not cache results from this action
        headers['Cache-Control'] = 'no-cache'
        # let the browser know that this file is a CSV
        headers['Content-Type'] = 'text/csv'
        # set the file name
        headers['Content-Disposition'] = 'attachment; filename="stream_user.csv"'
        # https://stackoverflow.com/questions/20926658/how-do-i-stream-a-generated-file-for-download-in-rails-4
        headers['Last-Modified'] = Time.current.httpdate

        csv_enumerator = Enumerator.new do |csv|
          csv << User.column_names.to_csv # add headers to the CSV
          User.all.find_each do |record|
            csv << record.attributes.values_at(*User.column_names).to_csv
          end
        end
        self.response_body = csv_enumerator
      end
    end
  end
  # END

  private

  def generate_csv(column_names, records)
    CSV.generate do |csv|
      csv << column_names # add headers to the CSV

      records.find_each do |record|
        csv << record.attributes.values_at(*column_names)
      end
    end
  end

  # BEGIN
  # END

  def user_params
    params.require(:user).permit(
      :name,
      :email
    )
  end
end
