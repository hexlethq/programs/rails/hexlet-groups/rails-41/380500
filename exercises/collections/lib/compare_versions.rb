# frozen_string_literal: true

# BEGIN

def compare_versions(ver_a, ver_b)
  major_a, minor_a = ver_a.split('.').map(&:to_i)
  major_b, minor_b = ver_b.split('.').map(&:to_i)

  if major_a > major_b
    1
  elsif major_a < major_b
    -1
  elsif minor_a > minor_b
    1
  elsif minor_a < minor_b
    -1
  else
    0
  end
end

# END
