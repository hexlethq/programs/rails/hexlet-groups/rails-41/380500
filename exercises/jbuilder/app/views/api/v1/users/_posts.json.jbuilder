# frozen_string_literal: true

json.(post, :id, :title)
