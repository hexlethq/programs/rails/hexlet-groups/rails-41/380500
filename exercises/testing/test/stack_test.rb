# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_empty
    assert_equal true, @stack.empty?
  end

  def test_push
    @stack.push! 'apple'
    @stack.push! 'banana'
    assert_equal false, @stack.empty?
    assert_equal 2, @stack.size
  end

  def test_pop
    @stack.push! 'apple'
    @stack.push! 'banana'
    assert_equal 2, @stack.size
    @stack.pop!
    assert_equal 1, @stack.size
  end

  def test_pop_when_stack_is_empty
    assert_equal true, @stack.empty?
    assert_nil @stack.pop!
  end

  def test_to_a
    @stack.push! 'apple'
    @stack.push! 'banana'
    assert_equal %w[apple banana], @stack.to_a
  end

  def test_clear!
    @stack.push! 'apple'
    assert_equal false, @stack.empty?
    @stack.clear!
    assert_equal true, @stack.empty?
  end

  def test_size
    assert_equal 0, @stack.size
    @stack.push! 'apple'
    assert_equal 1, @stack.size
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
