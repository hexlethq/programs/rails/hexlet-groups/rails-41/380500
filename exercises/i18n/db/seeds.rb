# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

4.times do |post_index|
  post = Post.create(
    title: "Post #{post_index}",
    body: "Body of Post #{post_index}"
  )
  case post_index
  when 1
    comment = post.comments.build(body: "A single comment for Post #{post_index}")
    comment.save
  when 2
    2.times do |comment_index|
      comment = post.comments.build(body: "Comment number #{comment_index} for Post #{post_index}")
      comment.save
    end
  when 3
    10.times do |comment_index|
      comment = post.comments.build(body: "Comment number #{comment_index} for Post #{post_index}")
      comment.save
    end
  end
end
