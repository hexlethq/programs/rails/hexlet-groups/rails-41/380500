require "test_helper"

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)
  end

  test 'should get index' do
    get tasks_path
    assert_response :success
  end

  test 'should get task' do
    get task_path(@task)
    assert_response :success
  end

  test 'should create task' do
    assert_difference('Task.count', +1) do
      post tasks_path, params: {
        task: {
          name: 'three',
          description: 'task #3'
        }
      }
      assert_response :redirect
      follow_redirect!
      assert_response :success
    end
  end

  test 'should edit task' do
    get edit_task_path(@task)
    assert_response :success
  end

  test 'should update task' do
    patch task_path(@task), params: {
      task: {
        name: 'dog'
      }
    }
    assert_redirected_to task_path(@task)
    follow_redirect!
    assert_response :success
    assert_select 'h1', 'dog'
  end

  test 'should delete task' do
    assert_difference('Task.count', -1) do
      delete task_path(@task)
    end
  end
end
